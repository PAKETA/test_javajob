package ru.vasilyev;

public class Task10N1056 {
    public static boolean recursion(int n, int i) {
        if (n <= 2) {
            return true;
        } else if (n % i == 0) {
            return false;
        } else if (i < n / 2) {
            return recursion(n, i + 1);
        } else {
            return true;
        }
    }

    public static void main(String[] args) {
        System.out.println("Число n простое - " + recursion(5, 2));
    }
}
