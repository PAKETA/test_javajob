package ru.vasilyev;

import java.util.Scanner;

public class Task04N436 {
    public static void main(String[] args) {
        int t, a, b;
        Scanner h = new Scanner(System.in);
        System.out.print("Введите прошедшее время в минутах с начала часа ");
        t = h.nextInt();
        a = t % 5;
        if ((a >= 0) && (a < 3))
            System.out.println("Горит зеленый цвет");
        else
            System.out.println("Горит красный цвет");
    }
}
