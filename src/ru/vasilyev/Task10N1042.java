package ru.vasilyev;

public class Task10N1042 {
    public static int power(int x, int y) {
        if (y > 1)
            return x * power(x, y - 1);
        else if (y == 1)
            return x;
        else if (y == 0)
            return 1;
        else
            return 0;
    }
    public static void main(String[] args) {
                System.out.println("2 в степени 3 = " + power(2, 3));
    }
}

