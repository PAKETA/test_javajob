package ru.vasilyev;

import java.util.Scanner;

public class Task12N1224 {
    public static void main(String[] args) {
        Scanner enter = new Scanner(System.in);
        System.out.println("Введите число N для массива N x N ");
        int N = enter.nextInt();

        int i, j = 0;
        int array[][] = new int[N][N];

        System.out.println("Task a");
        for (i = 0; i < N; i++) {
            array[i][0] = 1;
        }
        for (j = 0; j < N; j++) {
            array[0][j] = 1;
        }

        for (i = 1; i < N; i++)
            for (j = 1; j < N; j++) {
                array[i][j] = array[i - 1][j] + array[i][j - 1];
            }

        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }

        System.out.println("Task b");

         for (i = 0; i < N; i++)
            for (j = 0; j < N; j++) {
            array[i][j] = i+j+1;
                if (array[i][j]>6)
                    array[i][j]=array[i][j]%N;
            }


        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }

    }
}


