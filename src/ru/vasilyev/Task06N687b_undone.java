package ru.vasilyev;

public class Task06N687b_undone {
    public Team team1;
    public Team team2;

    public static void main(String[] args) {
        Task06N687b_undone task = new Task06N687b_undone();
             task.team1=new Team("Bulls");
        task.team2=new Team("Lakers");
        System.out.println( task.score(1,2));
        System.out.println( task.score(2,3));
        System.out.println( task.score(2,1));
        System.out.println( task.score(2,2));
            }

    public String score(int id, int score) {

        if (id == 1) {
            int currentScore = team1.getScore();
            team1.setScore(currentScore+score);
        }
        if (id == 2) {
            int currentScore = team2.getScore();
            team2.setScore(currentScore + score);
        }
        return team1.getScore()+" "+team2.getScore();
    }

    private static class Team {
        private String name;
        private int score = 0;

        public Team(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }


    }

}



