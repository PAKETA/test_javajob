package ru.vasilyev;


import java.util.Scanner;

public class Task02N213 {
    public static void main (String args[]) {
        int n, a, b, c;
        Scanner enter = new Scanner(System.in);
        System.out.print("Введите число ");
        n=enter.nextInt(); // считываем число с ввода и присваиваем переменной
        a = n/100; //первая цифра
        b = (n%100)/10; //вторая цифра
        c = n%10; //третья

        if (n>100 && n<200)
            System.out.print("Ваше число наоборот " +c+b+a);

    }
}
