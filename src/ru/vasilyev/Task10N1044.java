package ru.vasilyev;

public class Task10N1044 {
    static int task(int n) {
        if (n < 10) {
            return n;
        } else {
            return task(n % 10 + n / 10);
        }
    }

    public static void main(String[] args) {
        System.out.println(task(99));
    }
}
