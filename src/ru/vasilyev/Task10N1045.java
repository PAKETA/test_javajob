package ru.vasilyev;

public class Task10N1045 {
    static int taskA(int n, int a, int r) {
        if (n == 1) {
            return a;
        } else {
            return r + taskA(n - 1, a, r);
        }
    }

    static int taskB(int n, int a, int r) {
        if (n == 1) {
            return a;
        } else {
            return taskA(n, a, r) + taskB(n - 1, a, r);
        }
    }

    public static void main(String[] args) {
        System.out.println(taskA(3, 5, 10));
        System.out.println(taskB(3, 5, 10));
    }
}
