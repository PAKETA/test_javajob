package ru.vasilyev;

import java.util.Scanner;

public class Task02N231 {
    public static void main (String args[]){
        Scanner enter = new Scanner(System.in) ;
        System.out.print("Введите трехзначное число n ");
        int n, a, b, c;
        n=enter.nextInt(); // считываем число с ввода и присваиваем переменной
        a = n/100; //первая цифра
        b = (n%100)/10; //вторая цифра
        c = n%10; //третья
        System.out.println("Ваше число х = " +a +c +b);
    }
}
