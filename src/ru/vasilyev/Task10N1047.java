package ru.vasilyev;

public class Task10N1047 {
    static int task(int n) {
        if (n == 1) {
            return 0;
        }
        if (n == 2 || n == 3) {
            return 1;
        } else {
            return task(n - 1) + task(n - 2);
        }
    }

    public static void main(String[] args) {
        System.out.println(task(6));
    }
}
