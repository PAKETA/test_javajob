package ru.vasilyev;

import java.util.Scanner;

public class Task09N166 {
    public static void main(String[] args) {
        String word;
        Scanner h = new Scanner(System.in);
        System.out.println("Введите слово: ");
        word = h.nextLine();
        String[] parts = word.split(" ");
        String first = parts[0];
        parts[0] = parts[parts.length - 1];
        parts[parts.length - 1] = first;
        String result = String.join(" ", parts);
        System.out.println(result);
    }
}


