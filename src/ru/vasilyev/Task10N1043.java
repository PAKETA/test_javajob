package ru.vasilyev;

public class Task10N1043 {
static int taskA(int n){
    if (n < 10) {
        return n;
    }
    else {
        return n % 10 + taskA(n / 10);
    }
}
    static int taskB(int n){
        if (n < 10) {
            return 1;
        }
        else {
            return taskB(n / 10)+1;
        }
    }

    public static void main(String[] args) {
        System.out.println(taskA(123));
        System.out.println(taskB(1234));
    }
}
