package ru.vasilyev;

public class Task10N1051 {
    static int recA(int n) {
        if (n > 0) {
            System.out.println(n);
            return recA(n - 1);
        } else {
            return n;
        }
    }

    static int recB(int n) {
        if (n > 0) {
            System.out.println(recB(n-1));
            return n;
        } else {
            return n;
        }
    }

    static int recC(int n) {
        if (n > 0) {
            System.out.println(n);
            System.out.println(recC(n - 1));
            return n;
        } else {
            return n;
        }
    }

    public static void main(String[] args) {
        System.out.println(recA(5));
        System.out.println(recB(5));
        System.out.println(recC(5));
    }
}


