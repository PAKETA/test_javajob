package ru.vasilyev;

import java.util.Scanner;

public class Task04N467 {
    public static void main(String[] args) {
        int k, a, b;
        Scanner h = new Scanner(System.in);
        System.out.print("Введите число от 1 до 365 ");
        k = h.nextInt();
        a = k % 7;
        if ((a == 6) || (a == 0))
            System.out.println("Weekend");
        else
            System.out.println("Workday");

    }
}
