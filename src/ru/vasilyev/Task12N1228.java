package ru.vasilyev;

import java.util.Scanner;

public class Task12N1228 {
    public static void main(String[] args) {
        Scanner enter = new Scanner(System.in);
        System.out.println("Введите нечетное число N для массива N x N ");
        int N = enter.nextInt();

        int i = 1;
        int j, k;
        int array[][] = new int[N][N];

        if (N % 2 != 0) {
            for (k = 1; k < N; k++) {
                for (j = k - 1; j < N - k + 1; j++)
                    array[k - 1][j] = i++;

                for (j = k; j < N - k + 1; j++)
                    array[j][N - k] = i++;

                for (j = N - k - 1; j >= k - 1; --j)
                    array[N - k][j] = i++;

                for (j = N - k - 1; j >= k; j--)
                    array[j][k - 1] = i++;
            }

            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++)
                    System.out.print(array[i][j] + " ");
                System.out.println();
            }

        } else System.out.println("Ошибка. Вы ввели четное число!");
    }
}