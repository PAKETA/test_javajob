package ru.vasilyev;

import java.util.Scanner;

public class Task10N1053 {
    static Scanner sc = new Scanner(System.in);

    public static void rec() {
        int number = sc.nextInt();
        if (number == 0) {
            System.out.println(0);
        } else {
            rec();
            System.out.println(number);
        }
    }

    public static void main(String[] args) {
        System.out.println("Введите числа");
        rec();

    }
}




