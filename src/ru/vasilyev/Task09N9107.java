package ru.vasilyev;

import java.util.Scanner;

public class Task09N9107 {
    public static void main(String[] args) {
        StringBuilder word;
        Scanner h = new Scanner(System.in);
        System.out.println("Введите слово ");
        word = new StringBuilder(h.nextLine());
        int indexA = word.indexOf("a");
        int indexO = word.lastIndexOf("o");
        if (indexA != -1 && indexO != -1) {
            word.setCharAt(indexA, 'o');
            word.setCharAt(indexO, 'a');
            System.out.println(word);
        } else {
            System.out.println("Нет букв а и о в слове");
        }
    }
}


