package ru.vasilyev;

public class Task10N1046 {
    static int taskA(int n, int a, int q){
        if (n == 1) {
            return a;
        }
        else {
            return q * taskA(n-1, a,q);
        }
    }
    static int taskB(int n, int a, int q){
        if (n == 1) {
            return a;
        }
        else {
            return taskA(n,a,q) + taskB(n-1, a, q);
        }
    }

    public static void main(String[] args) {
        System.out.println(taskA(3,2,3));
        System.out.println(taskB(3,2,3));
    }
}

