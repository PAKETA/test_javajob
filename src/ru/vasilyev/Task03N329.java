package ru.vasilyev;

import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * @author Васильев Александр
 * <p>
 * Задача 3.29 Сборника Залотопольского.
 * <p>
 * Записать условие, которое является истинным, когда:
 * а) каждое из чисел X и Y нечетное;
 * б) только одно из чисел X и Y меньше либо равно 2;
 * в) хотя бы одно из чисел X и Y равно 0;
 * г) каждое из чисел X,Y,Z отрицательное;
 * д) только одно из чисел X,Y,Z кратно 5 и меньше либо равно 3;
 * е) хотя бы одно из чисел X,Y,Z больше 100;
 */

public class Task03N329 {
    public static void main(String[] args) {
        int x = 101, y = 101, z = 5;
        boolean f = true;

        if (x % 2 == 0 && y % 2 == 0)
            System.out.println("а) условие " + !f);
        else
            System.out.println("а) условие " + f);


        f = true;
        if (x <= 2 || y <= 2)
            System.out.println("б) условие " + f);
        else
            System.out.println("б) условие " + !f);

        f = true;
        if (x == 0 || y == 0)
            System.out.println("в) условие " + f);
        else
            System.out.println("в) условие " + !f);

        f = true;
        if (x < 0 && y < 0 && z < 0)
            System.out.println("г) условие " + f);
        else
            System.out.println("г) условие " + !f);


        f = true;
        if ((x % 5 == 0 & x <= 3) || (y % 5 == 0 && y <= 3) || (z % 5 == 0 && z <= 3))
            System.out.println("д) условие " + f);
        else
            System.out.println("д) условие " + !f);


        f = true;
        if (x > 100 || y > 100 || z > 100)
            System.out.println("е) условие " + f);
        else
            System.out.println("е) условие " + !f);
    }

}
