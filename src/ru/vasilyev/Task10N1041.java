package ru.vasilyev;

public class Task10N1041 {
    int fact(int n) {
        int result;
        if (n == 1) {
            return 1;
        }
        result = fact(n - 1) * n;
        return result;
    }

    public static void main(String[] args) {
        Task10N1041 f = new Task10N1041();
        System.out.println("Факториал числа n = " + f.fact(3));
    }
}
