package ru.vasilyev;

import java.util.Scanner;

public class Task04N4115 {
    public static void main(String[] args) {
        int m, a, b;
        String animal;
        String colour;
        Scanner h = new Scanner(System.in);
        System.out.print("Введите год нашей эры ");
        m = h.nextInt();
        a = m % 12;
        switch (a) {
            case 4:
                animal = "крыса";
                break;
            case 5:
                animal = "корова";
                break;
            case 6:
                animal = "тигр";
                break;
            case 7:
                animal = "заяц";
                break;
            case 8:
                animal = "дракон";
                break;
            case 9:
                animal = "змея";
                break;
            case 10:
                animal = "лошадь";
                break;
            case 11:
                animal = "овца";
                break;
            case 0:
                animal = "обезьяна";
                break;
            case 1:
                animal = "петух";
                break;
            case 2:
                animal = "собака";
                break;
            case 3:
                animal = "свинья";
                break;
            default:
                animal = "noname";
        }
        b = m % 10;
        switch (b) {
            case 4:
            case 5:
                colour = "зеленый";
                break;
            case 6:
            case 7:
                colour = "красный";
                break;
            case 8:
            case 9:
                colour = "желтый";
                break;
            case 0:
            case 1:
                colour = "белый";
                break;
            case 2:
            case 3:
                colour = "черный";
                break;
            default:
                colour = "no colour";


        }
        System.out.println(animal + " " + colour);
    }
}
