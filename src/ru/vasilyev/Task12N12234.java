package ru.vasilyev;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Random;
import java.util.Scanner;

public class Task12N12234 {
    public static void main(String[] args) {
        int i = 0;
        int j = 0;
        int array[][] = new int[5][5];
        Random N = new Random();

        System.out.println("TASK a");
        for (i = 0; i < 5; i++)
            for (j = 0; j < 5; j++) {
                array[i][j] = N.nextInt(10);
            }

        for (i = 0; i < 5; i++) {
            for (j = 0; j < 5; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }

        Scanner enter = new Scanner(System.in);
        System.out.println("Введите номер удаляемой строки: ");
        int k = enter.nextInt();

        for (i = (k - 1); i < 4; i++)
            for (j = 0; j < 5; j++) {
                array[i][j] = array[i + 1][j];
            }

        for (j = 0; j < 5; j++)
            array[i][j] = 0;

        for (i = 0; i < 5; i++) {
            for (j = 0; j < 5; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }

        System.out.println("TASK b");
        for (i = 0; i < 5; i++)
            for (j = 0; j < 5; j++) {
                array[i][j] = N.nextInt(10);
            }

        for (i = 0; i < 5; i++) {
            for (j = 0; j < 5; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }

        System.out.println("Введите номер удаляемого столбца: ");
        int s = enter.nextInt();

        for (i = 0; i < 5; i++)
            for (j = s - 1; j < 4; j++) {
                array[i][j] = array[i][j + 1];
            }

        for (i = 0; i < 5; i++)
            array[i][j] = 0;

        for (i = 0; i < 5; i++) {
            for (j = 0; j < 5; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }


    }
}
