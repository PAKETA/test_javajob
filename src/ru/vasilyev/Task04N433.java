package ru.vasilyev;

import java.util.Scanner;

public class Task04N433 {
    public static void main(String[] args) {
        int n, a, b;
        Scanner h = new Scanner(System.in);
        System.out.print("Введите натуральное число ");
        n = h.nextInt();
        a = n % 2;
        if (a == 0)
            System.out.println(n + " заканчивается чётной цифрой");
        else
            System.out.println(n + " заканчивается нечётной цифрой");

    }
}
