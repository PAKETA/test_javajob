package ru.vasilyev;

import java.util.Scanner;


public class Task10N1048_1049 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите размер массива: ");
        int size = sc.nextInt();
        int index = 0;
        double arr[] = new double[size];
        System.out.println("Введите элементы массива: ");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextDouble();
        }
        System.out.println("Максимальный элемент массива = " + maxArrElemCalc(arr, index));
        System.out.println("Индекс макс.элемента: " + indexElemCalc(arr, index, maxArrElemCalc(arr, index)));
    }

    private static double maxArrElemCalc(double[] arr, int index) {
        if (index < arr.length) {
            double next = maxArrElemCalc(arr, index + 1);
            if (arr[index] > next) {
                return arr[index];
            } else
                return next;
        }
        return arr[index - 1];
    }

    private static int indexElemCalc(double[] arr, int index, double val) {
        if (index < arr.length) {
            int next = indexElemCalc(arr, index + 1, val);
            if (arr[index] == val) {
                return index;
            } else
                return next;
        }
        return index - 1;
    }
}