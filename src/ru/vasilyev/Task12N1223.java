package ru.vasilyev;

import java.util.Scanner;

public class Task12N1223 {
    public static void main(String[] args) {
        Scanner enter = new Scanner(System.in);
        System.out.println("Введите нечетное число N для массива N x N ");
        int N = enter.nextInt();

        int i, j = 0;
        int array[][] = new int[N][N];

        if (N % 2 != 0) {
            System.out.println("Task a");
            for (i = 0; i < N; i++)
                for (j = 0; j < N; j++) {
                    if ((j == i) || (i + j == N - 1)) {
                        array[i][j] = 1;
                    } else
                        array[i][j] = 0;
                }
            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++)
                    System.out.print(array[i][j] + " ");
                System.out.println();
            }

            System.out.println("Task b");
            for (i = 0; i < N; i++)
                for (j = 0; j < N; j++)

                {
                    if ((j == i) || (i + j == N - 1) || ((j - (N / 2) == 0) || (i - (N / 2) == 0))) {
                        array[i][j] = 1;
                    } else
                        array[i][j] = 0;
                }
            for (i = 0; i < N; i++)

            {
                for (j = 0; j < N; j++)
                    System.out.print(array[i][j] + " ");
                System.out.println();
            }
            System.out.println("Task v");
            for (i = 0; i < N; i++)
                for (j = 0; j < N; j++)

                {
                    if (((j >= i) && (j < N - i)) || ((j <= i) && (j >= N - 1 - i))) {
                        array[i][j] = 1;
                    } else
                        array[i][j] = 0;
                }
            for (i = 0; i < N; i++)

            {
                for (j = 0; j < N; j++)
                    System.out.print(array[i][j] + " ");
                System.out.println();
            }
        } else System.out.println("Ошибка. Вы ввели четное число!");
    }
}
