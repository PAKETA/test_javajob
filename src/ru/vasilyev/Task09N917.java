package ru.vasilyev;

import java.util.Scanner;

public class Task09N917 {
    public static void main(String[] args) {
        String word;
        char first, last;
        int k;

        Scanner h = new Scanner(System.in);
        System.out.println("Введите слово: ");
        word = h.nextLine();
        first = word.charAt(0);
        k = word.length();
        last = word.charAt(k - 1);

        if (first == last) {
            System.out.println("First and Last are equals");
        }
        else {
            System.out.println("First and Last are not equals");
        }

    }
}
