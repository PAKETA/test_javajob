package ru.vasilyev;

import java.util.Scanner;

public class Task09N942 {
    public static void main(String[] args) {
        StringBuilder word;
        Scanner h = new Scanner(System.in);
        System.out.println("Введите слово ");
        word = new StringBuilder(h.nextLine());
        System.out.println(word.reverse());
    }
}
