package ru.vasilyev;

public class Task05N538 {
    public static void main(String[] args) {
        double x = 1;
        double r = 0, s = 1;
        while (x < 101) {
            s = 1 / x;
            r = r + s;
            x++;
        }
        System.out.println("Общий путь составляет " + r + " км");

        double z = 2, y = 1;
        double r1 = 0, s1 = 1, s2 = 2;
        while (y < 101) {
            s1 = 1 / y;
            s2 = 1 / z;
            r1 = r1 + s1 - s2;
            z = z + 2;
            y = y + 2;
        }
        System.out.println("Муж будет на расстоянии " + r1 + " км от дома");
    }
}
