package ru.vasilyev;

import java.util.Scanner;

public class Task04N4106 {
    public static void main(String[] args) {
        int m, a, b;
        String season;
        Scanner h = new Scanner(System.in);
        System.out.print("Введите месяц от 1 до 12 ");
        m = h.nextInt();
        switch (m) {
            case 12:
            case 1:
            case 2:
                season = "зиме";
                break;
            case 3:
            case 4:
            case 5:
                season = "весне";
                break;
            case 6:
            case 7:
            case 8:
                season = "лету";
                break;
            case 9:
            case 10:
            case 11:
                season = "осени";
                break;
            default:
                season = "несуществующему месяцу";
        }
        System.out.println("Введенный месяц " + m + " относится к " + season);
    }
}
