package ru.vasilyev;

import java.util.Scanner;

public class Task05N564 {
    public static void main(String[] args) {
        int i, j, s, p = 1, k = 1;
        Scanner h = new Scanner(System.in);
        System.out.print("Введите норматив площади  ");
        s = h.nextInt();
        System.out.print("Введите норматив количества людей  ");
        p = h.nextInt();

        int summx = 0;
        System.out.println("Площади районов: ");
        // создаем массив площадей - x
        int x[][] = new int[3][4];
        for (i = 0; i < 3; i++)
            for (j = 0; j < 4; j++) {
                x[i][j] = k;
                k++;
                summx += x[i][j];
            }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 4; j++)
                System.out.print(x[i][j] * s + " ");
            System.out.println();
        }
        System.out.println("Общая площадь районов = " + summx * s + " км");

        int summy = 0;
        System.out.println("Количество жителей в районах: ");
        // создаем массив людей - y
        int n = 1;
        int y[][] = new int[3][4];
        for (i = 0; i < 3; i++)
            for (j = 0; j < 4; j++) {
                y[i][j] = n;
                n++;
                summy += y[i][j];
            }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 4; j++)
                System.out.print(y[i][j] * p + " ");
            System.out.println();
        }
        System.out.println("Суммарная численность = " + summy * p + " чел");
        // вычисляем среднюю плотность в целом по области
        int mid = (summy * p) / (summx * s);
        System.out.println("Средняя плотность по области = " + mid + " чел/км");

    }
}
