package ru.vasilyev;


import java.util.Scanner;

public class Task04N415 {
    public static void main(String[] args) {
        int age, m, y, mtoday, ytoday;
      /* m - месяц рождения человека,
         y - год рождения человека,
         mtoday - текущий месяц,
         ytoday - текущий год.
         */
        Scanner h = new Scanner(System.in);
        System.out.print("Введите месяц рождения ");
        m = h.nextInt();
        System.out.print("Введите год рождения ");
        y = h.nextInt();
        System.out.print("Введите текущий месяц  ");
        mtoday = h.nextInt();
        System.out.print("Введите текущий год  ");
        ytoday = h.nextInt();

        if (mtoday >= m)
            age = ytoday - y;
        else
            age = ytoday - y - 1;
        System.out.println("Возраст = " + age + " лет");

    }
}
